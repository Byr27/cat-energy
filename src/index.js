import Vue from 'vue';

import 'normalize.css';
import 'flexboxgrid';

import App from './App';
import router from './router';

const app = new Vue({
  router,
  render: h => h(App),
});

app.$mount('#app');
