export default [
  {
    id: 1,
    image: 'src/images/products/catalog-1-desktop.png',
    name: 'CAT ENERGY PRO 500 Г',
    volume: 500,
    taste: 'Курица',
    price: 500,
  },
  {
    id: 2,
    image: 'src/images/products/catalog-2-desktop.png',
    name: 'CAT ENERGY PRO 1000 Г',
    volume: 1000,
    taste: 'Курица',
    price: 300,
  },
  {
    id: 3,
    image: 'src/images/products/catalog-3-desktop.png',
    name: 'CAT ENERGY PRO 500 Г',
    volume: 500,
    taste: 'Рыба',
    price: 500,
  },
  {
    id: 4,
    image: 'src/images/products/catalog-4-desktop.png',
    name: 'CAT ENERGY PRO 1000 Г',
    volume: 1000,
    taste: 'Рыба',
    price: 500,
  },
  {
    id: 5,
    image: 'src/images/products/catalog-5-desktop.png',
    name: 'CAT ENERGY SLIM 500 Г',
    volume: 500,
    taste: 'Гречка',
    price: 300,
  },
  {
    id: 6,
    image: 'src/images/products/catalog-6-desktop.png',
    name: 'CAT ENERGY SLIM 1000 Г',
    volume: 1000,
    taste: 'Гречка',
    price: 500,
  },
  {
    id: 7,
    image: 'src/images/products/catalog-7-desktop.png',
    name: 'CAT ENERGY SLIM 500 Г',
    volume: 500,
    taste: 'Рис',
    price: 300,
  },
];
